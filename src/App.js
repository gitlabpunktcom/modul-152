import React from "react";
import './App.css';
import Home from './home/home';
import Videoelement from "./videoelement/videoelement";
import Bildergalerie from "./bildergalerie/bildergalerie";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container-fluid">
            <a className="navbar-brand" href="/">Modul 152</a>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <Link className="nav-link" to="/">Home</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/videoelement">Videoelement</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/bildergalerie">Bildergalerie</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="container">
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/videoelement">
              <Videoelement />
            </Route>
            <Route path="/bildergalerie">
              <Bildergalerie />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
