import ImageUploader from 'react-images-upload';
import ImageGallery from 'react-image-gallery';
import './bildergalerie.css';

function Bildergalerie() {

  const images = [
    '/images/landscape1',
    '/images/landscape7',
    '/images/landscape3',
    '/images/landscape6',
    '/images/landscape5',
    '/images/landscape1',
    '/images/landscape2',
    '/images/landscape3',
    '/images/landscape7',
  ]

  const images2 = [
    {
      original: '/images/landscape1.jpg',
      thumbnail: '/images/landscape1_tn.jpg',
    },
    {
      original: '/images/landscape2.jpg',
      thumbnail: '/images/landscape2_tn.jpg',
    },
    {
      original: '/images/landscape3.jpg',
      thumbnail: '/images/landscape3_tn.jpg',
    },
    {
      original: '/images/landscape4.jpg',
      thumbnail: '/images/landscape4_tn.jpg',
    },
    {
      original: '/images/landscape5.jpg',
      thumbnail: '/images/landscape5_tn.jpg',
    },
    {
      original: '/images/text.jpg',
      thumbnail: '/images/text.jpg',
    },
    {
      original: '/images/pepejam.gif',
      thumbnail: '/images/pepejam.gif',
    },
  ]

  const imageThumbnail = images.map(image => 
  <a href={image+".jpg"}>
    <img src={image+"_tn.jpg"} height="200"></img>
  </a>
  )

  return (
    <div className="Bildergalerie">
      <h1>Bildergalerie</h1>
      {imageThumbnail}
      <ImageGallery items={images2} />
      <ImageUploader
                withIcon={true}
                buttonText='Choose images'
                imgExtension={['.jpg', '.gif', '.png', '.gif']}
                maxFileSize={5242880}
            />
    </div>
  );
}

export default Bildergalerie;
