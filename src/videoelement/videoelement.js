
function Videoelement() {
  return (
    <div className="Videoelement">
        <h1>Videoelement</h1>
        <video width="800" height="450" controls>
          <source src="/videos/video-project.mp4" type="video/mp4"/>
        </video>
    </div>
  );
}

export default Videoelement;
